from pycwr.io import read_auto
import numpy as np
import os
from pycwr.draw.RadarPlot import Graph, GraphMap
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg

# 函数：生成多层仰角下的反射率矩阵( return Q_ALL )
def N_generate(PRD):
    # 获取仰角数量
    global width, width_0, height_0, sweep_num
    sweep_num = len(PRD.scan_info.sweep.values)
    width = height = int(PRD.fields[0].x.max().values / 1000)  # 因为基数据使用米(m)表示的，而实际公式是千米(km)，所以需要除以1000作为公式转换
    # 因为一般来说第0层图像反射率值最全面，覆盖范围最广，而我们希望9层仰角的图像一致，因此选择第0层的参数作为宽、高
    width_0 = height_0 = 2 * width  # 这里相当于把坐标范围平移到第一象限

    Q_ALL = np.zeros((sweep_num, width_0, height_0))  # 将所有层的仰角的反射率结果存在一个数组里
    # print(Q.shape)
    ################################################################################################################

    for u in range(sweep_num):
        Q_list = [[-33] * width_0] * height_0  # 创建一个width行height列的二维表列
        Q = np.array(Q_list)  # 将二维表列转换为二维数组
        x = PRD.fields[u].x.values
        y = PRD.fields[u].y.values
        dBZ = PRD.fields[u].dBZ.values

        a = x / 1000 + width  # 将一维数组x中的每个元素都加width，这样可以将x轴的所有负值消去，生成新的数组a存放横轴值
        b = y / 1000 + height  # 将一维数组y中的每个元素都加height，这样可以将y轴的所有负值消去，生成新的数组a存放纵轴值
        a = a.astype(np.int)
        b = b.astype(np.int)

        for i in range(dBZ.shape[0]):  # 获取列数
            for j in range(dBZ.shape[1]):  # 获取行数
                try:
                    # print("Q[a[i][j]][b[i][j]]",Q[a[i][j]][b[i][j]])
                    Q[a[i][j]][b[i][j]] = dBZ[i][j]
                except:
                    pass  # 这里使用except主要是因为dBZ矩阵里面有很多nan，这里就相当于不管nan，因为到Q矩阵这些nan的位置为-33，也是没有值（不处理）的意思

        Q = np.rot90(Q, 1)  # 矩阵Q就是所得到的最后生成的反射率图像矩阵
        #print(Q.shape)
        Q_ALL[u, :, :] = Q
    #print("ALL layers: ' dBZ fininshed!")
    #print("Q_ALL.shape: ", Q_ALL.shape)

    return Q_ALL

# 函数：生成多层仰角下的其他特征值( return Feature_basic )
def feature_basic_generate(PRD,file):
    sweep_ = PRD.scan_info.fixed_angle.values  # 各个仰角值,一共有9个
    latitude_central = PRD.scan_info.latitude.values    # 中心纬度
    longitude_central = PRD.scan_info.longitude.values  # 中心经度
    width = height = int(PRD.fields[0].x.max().values / 1000)
    width_0 = height_0 = 2 * width  # 笛卡尔坐标系-宽\高(width)(图像宽\高)
    sweep_num = len(PRD.scan_info.sweep)  # 仰角层数（值）
    lat_0 = PRD.fields[0].lat.max().values  # 起始纬度
    lon_0 = PRD.fields[0].lon.min().values  # 起始经度
    print("###################################",lat_0,lon_0)
    lon_api = (longitude_central - lon_0) / height  # 经度分辨率
    lat_api = (latitude_central - lat_0) / width  # 经度分辨率

    Feature_basic = np.zeros((10, 9))# *注意：当不做冰雹分析的时候，这个空矩阵的维度和长度一般会发生变化

    Feature_basic[0] = sweep_  # 各个仰角值,一共有9个
    Feature_basic[1][0] = latitude_central  # 中心纬度
    Feature_basic[2][0] = longitude_central  # 中心经度
    Feature_basic[3][0] = width_0  # 笛卡尔坐标系-宽(width)(图像宽)
    Feature_basic[4][0] = height_0  # 笛卡尔坐标系-高(height)（图像高）
    Feature_basic[5][0] = sweep_num  # 仰角层数（值）
    Feature_basic[6][0] = lon_0  # 起始经度
    Feature_basic[7][0] = lat_0  # 起始纬度
    Feature_basic[8][0] = lon_api  # 经度分辨率
    Feature_basic[9][0] = lat_api  # 纬度分辨率
    # ***** 附加信息存储：
    #cc = str(PRD.scan_info.end_time.values)  # 输出该基数据的主要信息特征
    #print("cc",cc)
    Feature_basic[5][1] = file[-25:-21]  # 年
    print("file[-21:-17] ",file[-25:-21] )
    Feature_basic[5][2] = file[-21:-19]  # 月
    print("file[-21:-17] ",file[-21:-19] )
    Feature_basic[5][3] = file[-19:-17]  # 日
    Feature_basic[5][4] = file[-17:-15]  # 时
    Feature_basic[5][5] = file[-15:-13]   # 分
    #Feature_basic[5][1] = int(cc[0:4])  # 年
    #Feature_basic[5][2] = int(cc[5:7])  # 月
    #Feature_basic[5][3] = int(cc[8:10])  # 日
    #Feature_basic[5][4] = int(cc[11:13])  # 时
    #Feature_basic[5][5] = int(cc[14:16])  # 分
    print("Feature_basic -------->",Feature_basic[5])
    return Feature_basic

N = 100
num = 0
succeed = 0
file_dir = "/mnt/PRESKY/user/chaiwentao/Hail/run/in/spc/Rain"
Q_ALL_N = np.zeros((N, 9, 458, 458))  # 9层仰角反射率矩阵
#Q_RGB_N = np.zeros((N, 458, 458, 4))     # 组合反射率RGB矩阵
Feature_basic_N = np.zeros((N, 10, 9))   # 产品特征要素矩阵


for root, dirs, files in os.walk(file_dir):
    for file in files:
        num += 1
        file_path = os.path.join(root, file)
        try:
            PRD = read_auto(file_path)
            Q_ALL = N_generate(PRD)
            Feature_basic = feature_basic_generate(PRD, file)
            Q_ALL_N[succeed, :, :, :] = Q_ALL
            Feature_basic_N[succeed, :, :] = Feature_basic
            succeed += 1
            print("num: %d,succeed: %d" % (num, succeed))
        except:
            pass

np.savez('/mnt/PRESKY/user/chaiwentao/Hail/run/in/Q_N.npz', Q_array=Q_ALL_N)
np.savez('/mnt/PRESKY/user/chaiwentao/Hail/run/in/Feature_N.npz', Feature_array=Feature_basic_N)
#Q_RGB_ALL = np.load('/mnt/PRESKY/user/chaiwentao/Hail/run/in/Q_RGB_N.npz')['Q_RGB_array']
#np.savez('/mnt/PRESKY/user/chaiwentao/Hail/run/in/Q_RGB_N.npz', Q_RGB_array=Q_RGB_ALL)


print('ALL Data saved.')